<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use app\models\Post;
class PostController extends ActiveController
{
    public $enableCsrfValidation = false;
    public $modelClass = 'app\models\Post';
    
    public function actions()  
    {  
        $actions = parent::actions();  
        unset($actions['view']);
        return $actions;
    }  

    public function actionView($id){
        return $this->findModel($id);
    }
    public function findModel($id){
        $modelClass = $this->modelClass;
        if (($model = $modelClass::findAll(['pid'=>$id])) !== null) {  
            return $model;  
        }
    }
}
