<?php

namespace app\controllers;

use Yii;
use app\models\Userform;
use yii\filters\AccessControl;

class UsersController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login','signup'],
                        'roles' => ['?'] //not yet authenticated
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'] //has been authenticated
                    ],
                ],
            ]
        ];
    }
    public function actionLogin()
    {
        $model = new Userform();
        if ($model->load(Yii::$app->request->post())) {
            if($user = $model->login()){
                Yii::$app->user->login($user);
                return $this->goHome();
            }
            else{
                return $this->render('login', ['status' => 403,'model'=>$model,'message'=>'account or password is wrong.']);
            }
        }
        return $this->render('login', ['model'=>$model]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new Userform();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($user = $model->regist()) {
                Yii::$app->user->login($user);
                return $this->goHome();
            }
        }
        return $this->render('regist', ['model' => $model]);
    }
}
