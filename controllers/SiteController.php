<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;

class SiteController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['add', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add', 'update'],
                        'roles' => ["@"]
                    ]
                ],
            ]
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionError()
    {
        return $this->render('error');
    }
    public function actionAdd()
    {
        return $this->render('add');
    }
    public function actionUpdate()
    {
        return $this->render('update');
    }
}
