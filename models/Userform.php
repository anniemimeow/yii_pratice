<?php
namespace app\models;
use yii\base\Model;
use app\models\User;
class Userform extends Model{
    public $account;
    public $pwd;
    public $name;
    public $pwd_confirm;
    public function rules(){
        return [
            [['account','pwd','name','pwd_confirm'],'required'],
            [['account','pwd','name'],'string','max'=>12],
            ['account',function(){
                if(User::findOne(['account'=>$this->account])) $this->addError('account','The account has been registed.');
            }],
            ['pwd_confirm','compare','compareAttribute'=>'pwd','message'=>'Password do not match']
        ];
    }
    /**
     * regist
     *
     * @return User[null on failed]
     */
    public function regist(){
        $user = new User;
        $user->account = $this->account;
        $user->pwd = \Yii::$app->getSecurity()->generatePasswordHash($this->pwd);
        $user->name = $this->name;
        $user->uid = \Yii::$app->security->generateRandomString(12);
        $user->authKey = \Yii::$app->security->generateRandomString(12);
        $user->accessToken = \Yii::$app->security->generateRandomString(12);
        return $user->save() ? $user : null;
    }
    public function login(){
        $user = User::findOne(['account'=>$this->account]);
        if($user == null) return null;
        return \Yii::$app->getSecurity()->validatePassword($this->pwd,$user->pwd)?$user:null;
    }
}