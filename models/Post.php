<?php

namespace app\models;

use Yii;
use app\models\User;
/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $pid
 * @property string $title
 * @property string @article
 * @property string $uid
 * @property string $timestamp
 *
 * @property Users $u
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pid', 'title','article', 'uid'], 'required'],
            [['timestamp'], 'safe'],
            [['pid', 'title', 'uid'], 'string', 'max' => 128],
            [['article'], 'string', 'max' => 256],
            [['uid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['uid' => 'uid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'title' => 'Title',
            'article' => 'Article',
            'uid' => 'Uid',
            'timestamp' => 'Timestamp',
        ];
    }

    /**
     * Gets query for [[U]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getU()
    {
        return $this->hasOne(User::className(), ['uid' => 'uid']);
    }
}
