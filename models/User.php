<?php

namespace app\models;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $uid
 * @property string $account
 * @property string $pwd
 * @property string $name
 * @property string $authKey
 * @property string $accessToken
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token'=>$token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['account'=>$username]);
    }

    public function getUId()
    {
        return $this->uid;
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid','account', 'pwd', 'name','authKey','accessToken'], 'required'],
            [['account', 'name'], 'string', 'max' => 12],
            [['uid','pwd','authKey','accessToken'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'account' => 'Account',
            'pwd' => 'Pwd',
            'name' => 'Name',
            'authKey' => 'authKey',
            'accessToken' => 'accessToken',
        ];
    }
}
