<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin()?>
<?=$form->field($model,'account')->label("帳號")?>
<?=$form->field($model,'pwd')->passwordInput()->label("密碼")?>
<?=$form->field($model,'pwd_confirm')->passwordInput()->label("二次密碼")?>
<?=$form->field($model,'name')->label("暱稱")?>
<?=Html::submitButton('register!',['class'=>'btn btn-success'])?>
<?php ActiveForm::end() ?>