<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin()?>
<h3><?php if(isset($message)) echo $message;?></h3>
<?=$form->field($model,'account')->label("帳號")?>
<?=$form->field($model,'pwd')->passwordInput()->label("密碼")?>
<?=Html::submitButton('Login',['class'=>'btn btn-success'])?>
<?php ActiveForm::end() ?>