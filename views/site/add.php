<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-3">標題</div>
        </div>
        <div class="row">
            <input id="title" class="col-8">
        </div>
        <div class="row">
            <div class="col-3">文章</div>
        </div>
        <div class="row">
            <textarea class="col-12" id="article"></textarea>
        </div>
        <div class="row">
            <div class="col-10"></div>
            <div class="col-2">
                <?= Html::button('送出', ['class'=>'btn btn-success','onclick' => "
                    $.ajax({
                        type:'POST',
                        url:'" . Url::toRoute(['post']) . "',
                        success: function(rs){
                            location.href='".Url::toRoute(['site/index'])."'
                        },
                    })
                "]) ?>
            </div>
        </div>
    </div>
</body>

</html>