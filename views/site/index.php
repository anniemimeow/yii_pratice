<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-4"><h3>Yii_Pratice</h3></div>
            <div class="col-2"><?=Html::a("新增",Url::toRoute(['site/add']))?></div>
        </div>
        <div class="row">
            <div class="col-2">#id</div>
            <div class="col-6">Title</div>
            <div class="col-2">User</div>
            <div class="col-2">Time</div>
        </div>
    </div>
</body>

</html>